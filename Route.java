// compile... javac Route.java
// run... java Route
//
import java.lang.Math;

public class Route{
    public static int manhattan(int xa,int ya,int xb,int yb){ // compute manhattan distance
        // https://xlinux.nist.gov/dads/HTML/manhattanDistance.html
        return (Math.abs(xa-xb) + Math.abs(ya-yb));
    }
    public static int checkNext(int []move){ // check next move
        int min=Integer.MAX_VALUE;
        int i=0;
        int pos=0;
        for(;i<4;i++){
            if(move[i]<min){
                min=move[i];
                pos=i;
            }
        }
        return pos;
    }
    public static int checkMove(int xa,int ya,int []visited, int []distance){ // check possible move
        if(xa<0){ // invalid x position
            return Integer.MAX_VALUE;
        }
        if(ya<0){ // invalid y position
            return Integer.MAX_VALUE;
        }
        if(xa>3){ // invalid x position
            return Integer.MAX_VALUE;
        }
        if(ya>3){ // invalid y position
            return Integer.MAX_VALUE;
        }
        if(visited[(ya*4) + xa] == 1){ // position already visited
            return Integer.MAX_VALUE;
        }
        return distance[(ya*4)+xa];
    }
    public static void enableVisited(int []visited, int pos){ // enable position as visited
        visited[pos]=1;
    }
    public static void setVisited(int []visited){ // initially all positions not visited
        visited[0]=1;
        visited[1]=0;
        visited[2]=0;
        visited[3]=0;
        visited[4]=0;
        visited[5]=0;
        visited[6]=0;
        visited[7]=0;
        visited[8]=0;
        visited[9]=0;
        visited[10]=0;
        visited[11]=0;
        visited[12]=0;
        visited[13]=0;
        visited[14]=0;
        visited[15]=0;
    }
    public static void setDirty(int []dirty){ // set position as dirty
        dirty[0]=0;
        dirty[1]=0;
        dirty[2]=1;
        dirty[3]=0;
        dirty[4]=0;
        dirty[5]=1;
        dirty[6]=0;
        dirty[7]=1;
        dirty[8]=1;
        dirty[9]=0;
        dirty[10]=1;
        dirty[11]=0;
        dirty[12]=1;
        dirty[13]=0;
        dirty[14]=1;
        dirty[15]=0;
    }
    public static void computeDistance(int []distance){ // compute distance each node from goal
        // manhattan distance used
        distance[0]=manhattan(0,0,3,3);
        distance[1]=manhattan(1,0,3,3);
        distance[2]=manhattan(2,0,3,3);
        distance[3]=manhattan(3,0,3,3);
        distance[4]=manhattan(0,1,3,3);
        distance[5]=manhattan(1,1,3,3);
        distance[6]=manhattan(2,1,3,3);
        distance[7]=manhattan(3,1,3,3);
        distance[8]=manhattan(0,2,3,3);
        distance[9]=manhattan(1,2,3,3);
        distance[10]=manhattan(2,2,3,3);
        distance[11]=manhattan(3,2,3,3);
        distance[12]=manhattan(0,3,3,3);
        distance[13]=manhattan(1,3,3,3);
        distance[14]=manhattan(2,3,3,3);
        distance[15]=0;
    }
    public static void main(String args[]){
        int ax=0; // start position
        int ay=0;
        int bx=1;
        int by=0;
        int cx=2;
        int cy=0;
        int dx=3;
        int dy=0;

        int ex=0;
        int ey=1;
        int fx=1;
        int fy=1;
        int gx=2;
        int gy=1;
        int hx=3;
        int hy=1;

        int ix=0;
        int iy=2;
        int jx=1;
        int jy=2;
        int kx=2;
        int ky=2;
        int lx=3;
        int ly=2;

        int mx=0;
        int my=3;
        int nx=1;
        int ny=3;
        int ox=2;
        int oy=3;
        int px=3; // goal position
        int py=3;

        int posx=0; // current position
        int posy=0;
        int dirt=0; // dirt collected
        int energy=100; // energy used
        int posn=0; // next position to move

        int []distance=new int[16]; // distance each node from goal
        int []dirty=new int[16]; // check if position has some dirt to clean
        int []visited=new int[16]; // check if position already visited
        int []moves=new int[4]; // check for possible moves

        computeDistance(distance);
        setDirty(dirty);
        setVisited(visited);

        for(;true==true;){
            System.out.println("posx: "+posx);
            System.out.println("posy: "+posy);
            if((posx==px) && (posy==py)){ // goal reached
                break;
            }
            moves[0]=checkMove(posx+1,posy,visited,distance);
            moves[1]=checkMove(posx-1,posy,visited,distance);
            moves[2]=checkMove(posx,posy+1,visited,distance);
            moves[3]=checkMove(posx,posy-1,visited,distance);
            posn=checkNext(moves);
            if(dirty[posy*4+posx]==1){
                dirt++; // clean up some dirt
                System.out.println("cleaning dirt... ");
            }
            if(posn==0){
                posx++;
                energy--;
                enableVisited(visited,posy*4+posx);
            }
            if(posn==1){
                posx--;
                energy--;
                enableVisited(visited,posy*4+posx);
            }
            if(posn==2){
                posy++;
                energy--;
                enableVisited(visited,posy*4+posx);
            }
            if(posn==3){
                posy--;
                energy--;
                enableVisited(visited,posy*4+posx);
            }
        }
        System.out.println("dirt cleaned: "+dirt);
        System.out.println("energy remaining: "+energy);
    }
}
